package data;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import domain.CtrlJugador;
import domain.Jugador;

public class CtrlJugadorBD implements CtrlJugador {

	/*
	 * Aquesta funcio fa un select de totes les columnes de la taula Jugador de
	 * la instancia amb username = "userN" i despres retorna una instancia de la
	 * classe Jugador creada a partir de les dades d'aquest select.
	 */
	@Override
	public Jugador getJugador(String userN) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Jugador.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory factory = config.buildSessionFactory();
		Session session = null;
		Transaction tx = null;
		Jugador j = null;
		try {
			session = factory.openSession();
			tx = session.beginTransaction();
			List<Object[]> data = (List<Object[]>) session.createQuery(
					"SELECT nom, cognom, username, pwd, email FROM Jugador J WHERE J.username = \'"
							+ userN + "\'").list();
			Object[] jugador = data.get(0);
			String nom = (String) jugador[0];
			String cognom = (String) jugador[1];
			String username = (String) jugador[2];
			String pwd = (String) jugador[3];
			String email = (String) jugador[4];
			j = new Jugador(nom, cognom, username, pwd, email);
			// j = (Jugador) session.load(Jugador.class, userN);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (HibernateException e1) {
				}
			}
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (HibernateException e) {
				}
			}
		}
		return j;
	}

}
