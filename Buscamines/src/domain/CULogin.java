package domain;

public class CULogin {

	public CULogin() {
	}

	public void login(String userN, String password) throws Exception {
		CtrlUsuariRegistrat cur = CtrlDataFactory.getInstance()
				.getCtrlUsuariRegitrat();
		UsuariRegistrat ur = cur.getUsuariRegistrat(userN);
		String pwd = ur.getPwd();
		if (!pwd.equals(password))
			throw new Exception("Password incorrecte");
	}

}
