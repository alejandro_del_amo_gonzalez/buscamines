package domain;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class CUJugarPartida {

	private Jugador jugador;
	private Partida partida;
	private ArrayList<CasellaDescoberta> casellesDescobertes;
	private int nCasDesc = 0;

	public CUJugarPartida() {
		casellesDescobertes = new ArrayList<CasellaDescoberta>();
		nCasDesc = 0;
	}

	public void ferAutenticacio(String userN, String password) throws Exception {
		CULogin cul = new CULogin();
		cul.login(userN, password);
		CtrlJugador cj = CtrlDataFactory.getInstance().getCtrlJugador();
		jugador = cj.getJugador(userN);
		/*
		 * La unica diferencia d'aquesta operacio respecte l'original es aquest
		 * throw exception.
		 */
		if (jugador == null)
			throw new Exception("L'usuari no es jugador");
	}

	public ArrayList<StatsNivell> obtenirNivells() {
		CUConsultarNivells cucn = new CUConsultarNivells();
		return cucn.consultarNivells();
	}

	public String crearPartida(String nomNivell) {
		Buscamines bm = Buscamines.getInstance();
		int id = bm.getID();
		/*
		 * L'obtenir Nivell el vam haver de posar davant del crear partida
		 * perque en el diagrama original ens vam equivocar i guardavem el
		 * nivell a CUJugarPartida en comptes de a Partida.
		 */
		CtrlDataFactory cdf = CtrlDataFactory.getInstance();
		CtrlNivell cn = cdf.getCtrlNivell();
		Nivell n = cn.getNivell(nomNivell);
		/*
		 * La creadora de Partida ara te mes parametres. Tota la part de crear
		 * les caselles i assignar-les a la partida, aixi com l'assignacio de
		 * les mines a les caselles i l'assignacio del jugador actual a la
		 * partida es fa en la creadora de Partida.
		 */
		partida = new Partida(id, n, jugador);
		jugador.assignarPartidaAct(partida);
		Buscamines.getInstance().incrementaID();
		/*
		 * Abans la operacio crearPartida no retornava res pero ara retorna un
		 * String amb el tipus d'estrategia que utilitza la partida i el valor
		 * maxim associat a l'estrategia (en aquest cas timeMax o throwsMax).
		 * Aixo ho fem per facilitar la creacio de la vista a la capa de
		 * presentacio.
		 */
		String result = partida.getStrategy();
		if (result == "time") {
			int timeMax = Buscamines.getInstance().getTimeMax();
			String time = Integer.toString(timeMax);
			result += " " + time;
		} else if (result == "throws") {
			int throwsMax = Buscamines.getInstance().getThrowsMax();
			String tirades = Integer.toString(throwsMax);
			result += " " + tirades;
		}
		return result;
	}

	public ResultPartida descobrirCasella(int numF, int numC) throws Exception {
		/*
		 * Ara mirem si la casella esta descoberta o marcada o te mina amb una
		 * sola operacio en comptes de tres.
		 */
		boolean teMina = partida.comprovarCasella(numF, numC);
		if (teMina)
			partida.setEstaAcabada(true);
		else {
			/*
			 * L'inicialitzacio del temps ara es fa a la creadora de Partida. No
			 * tenia cap sentit inicialitzar el temps cada cop que es descobris
			 * una casella que no tingues mina.
			 */
			partida.incNumTirades();
			nCasDesc += partida.descobreixCas(numF, numC, casellesDescobertes);
		}
		int files = partida.getFiles();
		int columnes = partida.getColumnes();
		int mines = partida.getMines();
		int punts = -1;
		if (nCasDesc == (files * columnes - mines)) {
			int timeIni = partida.getTimeIni();
			GregorianCalendar timeAct = new GregorianCalendar();
			int timeFi = (int) (timeAct.getTimeInMillis() / 1000);
			int time = timeFi - timeIni;
			partida.setEstaAcabada(true);
			partida.setEstaGuanyada(true);
			punts = partida.calculaPuntuacio(time);
			int id = partida.getIdPartida();
			/*
			 * Aqui ens faltava posar l'AdapterFactory.
			 */
			IMissatgeriaAdapter ma = AdapterFactory.getInstance()
					.getIMissatgeriaAdapter();
			String miss = "ID Partida: " + id + ", Puntuacio: " + punts;
			ma.enviaMissatge(miss, jugador.getEmail());
		}
		boolean estaAcabada = partida.isEstaAcabada();
		boolean estaGuanyada = partida.isEstaGuanyada();
		/*
		 * Abans sempre cridavem a canviaJugador i a canviarPartida, pero nomes
		 * les haviem de cridar quan la partida s'acabes.
		 */
		if (estaAcabada) {
			partida.canviaJugador(jugador);
			jugador.canviarPartida(partida);
		}
		/*
		 * Vam veure que no era necessari tenir un optatiu a l'hora de crear el
		 * resultat depenent de si era una partida guanyada o no.
		 */
		ResultPartida result = new ResultPartida(estaAcabada, estaGuanyada,
				punts, casellesDescobertes);
		return result;
	}

	public void marcarCasella(int numF, int numC) throws Exception {
		/*
		 * Ara utilitzem l'operacio comprovarCasella la qual abarca les
		 * comprovacions de si una casella ja esta descoberta o marcada.
		 */
		partida.comprovarCasella(numF, numC);
		partida.marcarCasella(numF, numC);
	}

	public void desmarcarCasella(int numF, int numC) throws Exception {
		/*
		 * Ara utilitzem l'operacio comprovarCasella la qual abarca les
		 * comprovacions de si una casella ja esta descoberta o marcada.
		 */
		partida.comprovarCasella(numF, numC);
		partida.desmarcarCasella(numF, numC);
	}

	/*
	 * Hem afegit aquesta operacio per facilitar l'obtencio de les dades a la
	 * capa de presentacio.
	 */
	public boolean isMarcada(int numF, int numC) {
		return partida.isMarcada(numF, numC);
	}
	
	public boolean isDescoberta(int numF, int numC) {
		return partida.isDescoberta(numF, numC);
	}
}
