package domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Buscamines {

	private int id;
	private int timeMax;
	private int throwsMax;
	private static Buscamines instance;

	private Buscamines() {
		CtrlBuscamines cb = CtrlDataFactory.getInstance().getCtrlBuscamines();
		// id = cb.getIDBuscamines();
		id = 0;
		// el temps maxim i els throws max es defineixen ara a aquesta classe
		timeMax = 1000;
		throwsMax = 300;
	}

	public static Buscamines getInstance() {
		if (instance == null)
			instance = new Buscamines();
		return instance;
	}

	@Id
	public int getID() {
		return id;
	}

	public void incrementaID() {
		++id;
	}

	public int getTimeMax() {
		return timeMax;
	}

	public int getThrowsMax() {
		return throwsMax;
	}

}
