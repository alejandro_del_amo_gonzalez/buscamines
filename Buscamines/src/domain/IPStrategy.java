package domain;
public interface IPStrategy {

	int calcularPuntuacio(int time, int nombreTirades);

	String getStrategy();

}
