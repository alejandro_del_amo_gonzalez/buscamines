package domain;
public class StatsNivell {

	private String nom;
	private int files;
	private int columnes;
	private int mines;

	public StatsNivell(String nom, int files, int columnes, int mines) {
		this.nom = nom;
		this.files = files;
		this.columnes = columnes;
		this.mines = mines;
	}

	public String getNom() {
		return nom;
	}

	public int getFiles() {
		return files;
	}

	public int getColumnes() {
		return columnes;
	}

	public int getMines() {
		return mines;
	}

}
