package data;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import domain.CtrlNivell;
import domain.Nivell;

public class CtrlNivellBD implements CtrlNivell {

	/*
	 * Aquesta funcio fa un select de totes les columnes de la taula Nivell de
	 * la instancia amb nom = "nomNivell" i despres retorna una instancia de la
	 * classe Nivell creada a partir de les dades d'aquest select.
	 */
	@Override
	public Nivell getNivell(String nomNivell) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Nivell.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory factory = config.buildSessionFactory();
		Session session = null;
		Transaction tx = null;
		Nivell n = null;
		try {
			session = factory.openSession();
			tx = session.beginTransaction();
			List<Object[]> data = session
					.createQuery(
							"SELECT nom, nombreCasellesxColumna, nombreCasellesxFila, nombreMines FROM Nivell N WHERE N.nom = \'"
									+ nomNivell + "\'").list();
			Object[] nivell = data.get(0);
			String nom = (String) nivell[0];
			int files = (int) nivell[1];
			int columnes = (int) nivell[2];
			int mines = (int) nivell[3];
			n = new Nivell(nom, files, columnes, mines);
			// nivell = (Nivell) session.load(Nivell.class, nomNivell);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (HibernateException e1) {
				}
			}
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (HibernateException e) {
				}
			}
		}
		return n;
	}

	/*
	 * Aquesta funcio fa un select de totes les columnes de la taula Nivell de
	 * totes les instancies i despres retorna un ArrayList de tipus Nivell amb
	 * totes aquestes instancies obtingudes amb el select.
	 */
	@Override
	public ArrayList<Nivell> all() {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Nivell.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory factory = config.buildSessionFactory();
		Session session = null;
		Transaction tx = null;
		ArrayList<Nivell> ns = new ArrayList<Nivell>();
		try {
			session = factory.openSession();
			tx = session.beginTransaction();
			List<Object[]> data = session
					.createQuery(
							"SELECT nom, nombreCasellesxColumna, nombreCasellesxFila, nombreMines FROM Nivell")
					.list();
			for (Object[] o : data) {
				String nom = (String) o[0];
				int files = (int) o[1];
				int columnes = (int) o[2];
				int mines = (int) o[3];
				Nivell nivell = new Nivell(nom, files, columnes, mines);
				ns.add(nivell);
			}
			// nivells.addAll(data);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (HibernateException e1) {
				}
			}
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (HibernateException e) {
				}
			}
		}
		return ns;
	}

}
