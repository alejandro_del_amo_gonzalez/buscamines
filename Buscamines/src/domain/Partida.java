package domain;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;

public class Partida {

	private int idPartida;
	private boolean estaAcabada;
	private boolean estaGuanyada;
	private int nombreTirades;
	private int timeIni;
	private Nivell nivell;
	private IPStrategy pStrategy;
	private ArrayList<Casella> caselles = new ArrayList<Casella>();
	private Jugador jugadorPartidaActual;
	private Jugador jugadorPartidaJugada;

	public Partida(int id, Nivell n, Jugador jugador) {
		idPartida = id + 1;
		estaAcabada = false;
		estaGuanyada = false;
		nombreTirades = 0;
		/*
		 * Ara tenim l'atribut nivell a Partida.
		 */
		nivell = n;
		pStrategy = TipusPuntFactory.getInstance().getIPStrategy();
		int files = n.getNombreCasellesxColumna();
		int columnes = n.getNombreCasellesxFila();
		int mines = n.getNombreMines();
		/*
		 * Aqui tenim la creacio de les caselles de la partida que abans feiem
		 * al CUJugarPartida, conjuntament amb l'assignacio de les mines a les
		 * caselles i del jugador actual a la partida.
		 */
		for (int i = 0; i < files; ++i) {
			for (int j = 0; j < columnes; ++j) {
				Casella c = new Casella(i, j);
				assignarCasella(c);
			}
		}
		assignarMines(files, columnes, mines);
		assignarJugadorAct(jugador);
		/*
		 * Ara la inicialitzacio del temps es fa aqui a la creadora de Partida i
		 * a mes el temps inicial es guarda a un atribut de la classe Partida.
		 */
		GregorianCalendar timeAct = new GregorianCalendar();
		timeIni = (int) (timeAct.getTimeInMillis() / 1000);
	}

	public void assignarCasella(Casella c) {
		caselles.add(c);
	}

	public void assignarMines(int files, int columnes, int mines) {
		int i = 0;
		while (i < mines) {
			Random rnd = new Random();
			int f = rnd.nextInt(files);
			int c = rnd.nextInt(columnes);
			Casella cas = caselles.get(f * columnes + c);
			if (!cas.getTeMina()) {
				cas.setTeMina(true);
				ArrayList<Casella> veins = obtenirVeins(f, c);
				for (Casella v : veins) {
					v.incrementaNum();
				}
				++i;
			}
		}
	}

	/*
	 * Aquesta funcio l'hem canviat for�a perque la que teniem era una mica
	 * ineficient perque feia coses innecesaries. Ara ens ajudem d'un parell de
	 * vectors auxiliars que ens indiquen que hem d'afegir a la fila i a la
	 * columna d'una casella per saber quins son els numeros de fila i columna
	 * dels veins d'aquesta casella. Mirem que cadascun dels veins no es trobi
	 * fora de rang de la matriu per saber si son veins valids i si ho son els
	 * afegim al resultat.
	 */
	public ArrayList<Casella> obtenirVeins(int numF, int numC) {
		int files = nivell.getNombreCasellesxColumna();
		int columnes = nivell.getNombreCasellesxFila();
		ArrayList<Casella> veins = new ArrayList<Casella>();
		int aux[] = { 0, 0, 1, -1, 1, -1, 1, -1 };
		int aux2[] = { 1, -1, 0, 0, 1, -1, -1, 1 };
		for (int i = 0; i < 8; ++i) {
			int filaCas = numF + aux[i];
			int colCas = numC + aux2[i];
			if ((filaCas >= 0) && (filaCas < files) && (colCas >= 0)
					&& (colCas < columnes)) {
				Casella cas = caselles.get((numF + aux[i]) * columnes + numC
						+ aux2[i]);
				if (!cas.getEstaDescoberta())
					veins.add(cas);
			}
		}
		return veins;
	}

	public void assignarJugadorAct(Jugador jugador) {
		jugadorPartidaActual = jugador;
	}

	/*
	 * Aquesta funcio abans no hi era i es la que compren la comprovacio de si
	 * una casella ja esta descoberta o ja esta marcada o te mina.
	 */
	public boolean comprovarCasella(int numF, int numC) throws Exception {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		if (cas.getEstaDescoberta())
			;
		if (cas.getEstaMarcada())
			;
		return cas.getTeMina();
	}

	public void incNumTirades() {
		nombreTirades++;
	}

	public int descobreixCas(int numF, int numC,
			ArrayList<CasellaDescoberta> casellesDesc) {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		/*
		 * En aquest funcio ens faltava comprovar si la casella ja estava
		 * descoberta per no tornarla a descobrir.
		 */
		if (!cas.getEstaDescoberta()) {
			cas.setEstaDescoberta(true);
			CasellaDescoberta casellaDesc = new CasellaDescoberta(
					cas.getNumeroFila(), cas.getNumeroColumna(),
					cas.getNumero());
			casellesDesc.add(casellaDesc);
			int nCasDesc = 1;
			if (!cas.getTeMina()) {
				int num = cas.getNumero();
				if (num == 0) {
					ArrayList<Casella> veins = obtenirVeins(numF, numC);
					for (Casella v : veins) {
						nCasDesc += descobreixCas(v.getNumeroFila(),
								v.getNumeroColumna(), casellesDesc);
					}
				}
			}
			return nCasDesc;
		}
		return 0;
	}

	public int calculaPuntuacio(int time) {
		int punts = pStrategy.calcularPuntuacio(time, nombreTirades);
		return punts;
	}

	public void canviaJugador(Jugador jugador) {
		jugadorPartidaActual = null;
		jugadorPartidaJugada = jugador;
	}

	public void marcarCasella(int numF, int numC) {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		cas.setEstaMarcada(true);
	}

	public void desmarcarCasella(int numF, int numC) {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		cas.setEstaMarcada(false);
	}

	/*
	 * Hem afegit aquesta operacio per facilitar l'obtencio de les dades a la
	 * capa de presentacio.
	 */
	public boolean isMarcada(int numF, int numC) {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		return cas.getEstaMarcada();
	}
	
	public boolean isDescoberta(int numF, int numC) {
		int columnes = nivell.getNombreCasellesxFila();
		Casella cas = caselles.get(numF * columnes + numC);
		return cas.getEstaDescoberta();
	}

	public int getIdPartida() {
		return idPartida;
	}

	public boolean isEstaAcabada() {
		return estaAcabada;
	}

	public void setEstaAcabada(boolean estaAcabada) {
		this.estaAcabada = estaAcabada;
	}

	public boolean isEstaGuanyada() {
		return estaGuanyada;
	}

	public void setEstaGuanyada(boolean estaGuanyada) {
		this.estaGuanyada = estaGuanyada;
	}

	public int getNombreTirades() {
		return nombreTirades;
	}

	public int getTimeIni() {
		return timeIni;
	}

	public Nivell getNivell() {
		return nivell;
	}

	public int getFiles() {
		return nivell.getNombreCasellesxColumna();
	}

	public int getColumnes() {
		return nivell.getNombreCasellesxFila();
	}

	public int getMines() {
		return nivell.getNombreMines();
	}

	public String getStrategy() {
		return pStrategy.getStrategy();
	}

}
