package domain;
public class TimePStrategy implements IPStrategy {
	//implementacio d'estrategia per temps, nomes es crea una sola vegada
	private static TimePStrategy instance;
	private int timeMax;

	private TimePStrategy() {
		timeMax = Buscamines.getInstance().getTimeMax();
	}

	public static TimePStrategy getInstance() {
		if (instance == null)
			instance = new TimePStrategy();
		return instance;
	}

	public int calcularPuntuacio(int time, int nombreTirades) {
		return timeMax - time;
	}

	public String getStrategy() {
		return "time";
	}

}
