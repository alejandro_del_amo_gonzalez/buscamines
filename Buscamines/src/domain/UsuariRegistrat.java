package domain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UsuariRegistrat {

	private String nom;
	private String cognom;
	private String username;
	private String pwd;

	public UsuariRegistrat(String nom, String cognom, String username,
			String pwd) {
		this.nom = nom;
		this.cognom = cognom;
		this.username = username;
		this.pwd = pwd;
	}

	@Column(nullable = false)
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(nullable = false)
	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	@Id
	@Column(nullable = false)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(nullable = false)
	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
