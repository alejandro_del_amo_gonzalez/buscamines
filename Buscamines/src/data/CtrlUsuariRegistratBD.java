package data;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import domain.CtrlUsuariRegistrat;
import domain.UsuariRegistrat;

public class CtrlUsuariRegistratBD implements CtrlUsuariRegistrat {

	/*
	 * Aquesta funcio fa un select de totes les columnes de la taula
	 * UsuariRegistrat de la instancia amb username = "userN" i despres retorna
	 * una instancia de la classe UsuariRegistrat creada a partir de les dades
	 * d'aquest select.
	 */
	@Override
	public UsuariRegistrat getUsuariRegistrat(String userN) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(UsuariRegistrat.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory factory = config.buildSessionFactory();
		Session session = null;
		Transaction tx = null;
		UsuariRegistrat ur = null;
		try {
			session = factory.openSession();
			tx = session.beginTransaction();
			List<Object[]> data = (List<Object[]>) session
					.createQuery(
							"SELECT nom, cognom, username, pwd FROM UsuariRegistrat UR WHERE UR.username = \'"
									+ userN + "\'").list();
			Object[] user = data.get(0);
			String nom = (String) user[0];
			String cognom = (String) user[1];
			String username = (String) user[2];
			String pwd = (String) user[3];
			ur = new UsuariRegistrat(nom, cognom, username, pwd);
			// ur = (UsuariRegistrat) session.load(UsuariRegistrat.class,
			// userN);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (HibernateException e1) {
				}
			}
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (HibernateException e) {
				}
			}
		}
		return ur;
	}

}
