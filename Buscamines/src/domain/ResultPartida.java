package domain;
import java.util.ArrayList;

public class ResultPartida {

	private boolean acabada;
	private boolean guanyada;
	private int puntuacio;
	private ArrayList<CasellaDescoberta> casellesDescobertes;

	public ResultPartida(boolean acabada, boolean guanyada, int puntuacio,
			ArrayList<CasellaDescoberta> casellesDescobertes) {
		this.acabada = acabada;
		this.guanyada = guanyada;
		this.puntuacio = puntuacio;
		this.casellesDescobertes = casellesDescobertes;
	}

	public boolean isAcabada() {
		return acabada;
	}

	public boolean isGuanyada() {
		return guanyada;
	}

	public int getPuntuacio() {
		return puntuacio;
	}

	public ArrayList<CasellaDescoberta> getCasellesDescobertes() {
		return casellesDescobertes;
	}

}
