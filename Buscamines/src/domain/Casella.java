package domain;
public class Casella {

	private int numeroFila;
	private int numeroColumna;
	private int numero;
	private boolean estaDescoberta;
	private boolean estaMarcada;
	private boolean teMina;

	public Casella(int i, int j) {
		this.numeroFila = i;
		this.numeroColumna = j;
		this.numero = 0;
		this.estaDescoberta = false;
		this.estaMarcada = false;
	}

	public int getNumeroFila() {
		return numeroFila;
	}

	public int getNumeroColumna() {
		return numeroColumna;
	}

	public int getNumero() {
		return this.numero;
	}

	public void incrementaNum() {
		this.numero++;
	}

	public boolean getEstaDescoberta() {
		return this.estaDescoberta;
	}

	public void setEstaDescoberta(boolean b) {
		this.estaDescoberta = b;
	}

	public boolean getEstaMarcada() {
		return this.estaMarcada;
	}

	public void setEstaMarcada(boolean b) {
		this.estaMarcada = b;
	}

	public boolean getTeMina() {
		return teMina;
	}

	public void setTeMina(boolean b) {
		this.teMina = b;
	}

}
