package domain;
public class AdapterFactory {

	private static AdapterFactory instance;
	private IMissatgeriaAdapter IMissatgeriaAdapter = null;

	private AdapterFactory() {
	}

	public static AdapterFactory getInstance() {
		if (instance == null)
			instance = new AdapterFactory();
		return instance;
	}

	public IMissatgeriaAdapter getIMissatgeriaAdapter() {
		if ( IMissatgeriaAdapter == null) IMissatgeriaAdapter = new MissatgeriaAdapter();
		return IMissatgeriaAdapter;
	}

}
