package domain;

import domain.MissatgeriaStub.EnviarMissatge;
import domain.MissatgeriaStub.EnviarMissatgeResponse;

//adaptador del servei de missatgeria
public class MissatgeriaAdapter implements IMissatgeriaAdapter {
	
	public MissatgeriaAdapter(){}
	
	public void enviaMissatge(String miss, String email) throws Exception {
		try {
			//establim la connexio amb el servei a traves del stub
			MissatgeriaStub stub = new MissatgeriaStub();
			EnviarMissatge envMiss = new EnviarMissatge();
			envMiss.setContingut(miss);
			envMiss.setEmail(email);
			EnviarMissatgeResponse emr = stub.enviarMissatge(envMiss);
			//retorna si ha estat possible enviar el missatge
			if (!emr.get_return()) throw new Exception();
		}
		catch (Exception e) {
			throw new Exception ("No ha estat possible enviar el missatge");
		}
	}

}
