package domain;
import java.util.ArrayList;

public class CUConsultarNivells {

	public CUConsultarNivells() {
	}

	public ArrayList<StatsNivell> consultarNivells() {
		CtrlNivell cn = CtrlDataFactory.getInstance().getCtrlNivell();
		ArrayList<Nivell> nivells = cn.all();
		ArrayList<StatsNivell> conjNivells = new ArrayList<StatsNivell>();
		for (Nivell n : nivells) {
			String nom = n.getNom();
			int files = n.getNombreCasellesxColumna();
			int columnes = n.getNombreCasellesxFila();
			int mines = n.getNombreMines();
			StatsNivell statsNivell = new StatsNivell(nom, files, columnes,
					mines);
			conjNivells.add(statsNivell);
		}
		return conjNivells;
	}

}
