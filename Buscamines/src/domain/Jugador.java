package domain;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Jugador extends UsuariRegistrat {

	private String email;
	private Partida partidaActual;
	private ArrayList<Partida> partidesJugades;

	public Jugador(String nom, String cognom, String user, String pwd,
			String email) {
		super(nom, cognom, user, pwd);
		this.email = email;
		partidesJugades = new ArrayList<Partida>();
	}

	public void assignarPartidaAct(Partida p) {
		this.partidaActual = p;
	}

	public void canviarPartida(Partida p) {
		this.partidaActual = null;
		partidesJugades.add(p);
	}

	@Column(nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
