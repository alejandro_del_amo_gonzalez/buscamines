package presentation;

import java.util.ArrayList;

import domain.CUJugarPartida;
import domain.ResultPartida;
import domain.StatsNivell;

public class JugarPartidaController {

	private CUJugarPartida CUJugarPartidaActual;
	private JugarPartidaView view;

	/* Creadora per defecte */
	public JugarPartidaController() {
		CUJugarPartidaActual = new CUJugarPartida();
	}

	

	/* Funcio que autentifica l'usuari i tambe retorna els nivells que hi ha en el sistema */
	
	public String[][] pressOKLogin(String user, String pass) throws Exception {
		CUJugarPartidaActual.ferAutenticacio(user, pass);
		ArrayList<StatsNivell> arrayNivells = CUJugarPartidaActual
				.obtenirNivells();
		String[][] nivells = new String[arrayNivells.size()][4];
		for (int i = 0; i < arrayNivells.size(); ++i) {
			nivells[i][0] = arrayNivells.get(i).getNom();
			nivells[i][1] = Integer.toString(arrayNivells.get(i).getFiles());
			nivells[i][2] = Integer.toString(arrayNivells.get(i).getColumnes());
			nivells[i][3] = Integer.toString(arrayNivells.get(i).getMines());
		}
		return nivells;
	}
	
	/* Funcio que crea la partida a partir del nivell que li passem com a parametre */
	
	public String pressOKConsNivell(String nivell) {
		return CUJugarPartidaActual.crearPartida(nivell);
	}

	/* Funcio que descobreix la casella en la fila = fil i columna = col, 
	 * i ens retorna un struct ResultPartida.
	 * El struct ResultPartida ens dona informacio sobre la partida,
	 * tal com si esta acabada, guanyada i quines son les caselles que en cal descubrir */
	
	public ResultPartida descobrirCasella(Integer fil, Integer col)
			throws Exception {
		return CUJugarPartidaActual.descobrirCasella(fil, col);
	}

	/* Funcio que marca una casella (possar una bandera) en la fila = fil i columna = col*/
	
	public void marcarCasella(Integer fil, Integer col) throws Exception {
		CUJugarPartidaActual.marcarCasella(fil, col);
	}

	/* Funcio que desmarca una casella (treu una bandera) en la fila = fil i columna = col*/
	
	public void desmarcarCasella(Integer fil, Integer col) throws Exception {
		CUJugarPartidaActual.desmarcarCasella(fil, col);
	}

	/* Funcio que retorna un boolean indicant si la casella en la fila = fil i columna = col,
	 * esta marcada o no */
	
	public boolean isMarcada(Integer fil, Integer col) {
		return CUJugarPartidaActual.isMarcada(fil, col);
	}
	/* Funcio que retorna un boolean indicant si la casella en la fila = fil i columna = col,
	 * esta descoberta o no */
		
	public boolean isDescoberta(Integer fil, Integer col) {
		return CUJugarPartidaActual.isDescoberta(fil, col);
	}

}
