package data;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import domain.Buscamines;
import domain.CtrlBuscamines;

public class CtrlBuscaminesBD implements CtrlBuscamines {

	/*
	 * Aquesta funcio fa un select de la columna id de la taula Buscamines de
	 * totes les instancies (nomes hi ha una) i retorna un integer que es l'id
	 * que s'obte amb el select.
	 */
	@Override
	public int getIDBuscamines() {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Buscamines.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory factory = config.buildSessionFactory();
		Session session = null;
		Transaction tx = null;
		int idBuscamines = 0;
		try {
			session = factory.openSession();
			tx = session.beginTransaction();
			List<Object[]> list = session.createQuery(
					"SELECT id FROM Buscamines").list();
			if (list.size() > 0) {
				Object[] obj = list.get(0);
				idBuscamines = (int) obj[0];
			}
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (HibernateException e1) {
				}
			}
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (HibernateException e) {
				}
			}
		}
		return idBuscamines;
	}

}
