package domain;

import data.CtrlBuscaminesBD;
import data.CtrlJugadorBD;
import data.CtrlNivellBD;
import data.CtrlUsuariRegistratBD;

public class CtrlDataFactory {
	/**
	 * static Singleton instance
	 */
	private static CtrlDataFactory instance;
	private CtrlNivell CtrlNivell;
	private CtrlJugador CtrlJugador;
	private CtrlUsuariRegistrat CtrlUsuariRegistrat;
	private CtrlBuscamines CtrlBuscamines;

	/**
	 * Private constructor for singleton
	 */
	private CtrlDataFactory() {
		CtrlNivell = new CtrlNivellBD();
		CtrlJugador = new CtrlJugadorBD();
		CtrlUsuariRegistrat = new CtrlUsuariRegistratBD();
		CtrlBuscamines = new CtrlBuscaminesBD();
	}

	/**
	 * Static getter method for retrieving the singleton instance
	 */
	public static CtrlDataFactory getInstance() {
		if (instance == null) {
			instance = new CtrlDataFactory();
		}
		return instance;
	}

	public CtrlNivell getCtrlNivell() {
		return CtrlNivell;
	}

	public CtrlJugador getCtrlJugador() {
		return CtrlJugador;
	}

	public CtrlUsuariRegistrat getCtrlUsuariRegitrat() {
		return CtrlUsuariRegistrat;
	}

	public CtrlBuscamines getCtrlBuscamines() {
		return CtrlBuscamines;
	}

}
