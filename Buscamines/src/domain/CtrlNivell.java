package domain;

import java.util.ArrayList;

public interface CtrlNivell {

	Nivell getNivell(String nomNivell);

	ArrayList<Nivell> all();

}
