package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import domain.ResultPartida;

public class JugarPartidaView implements ActionListener, MouseListener {

	private JugarPartidaController CtrlJugarPartida;
	private JFrame jframe;
	private JTextPane userField, txtPassword;
	private JTextField txtUser;
	private JPasswordField passwordField;
	private JButton nextButton, finishButton;
	private JRadioButton radioButton;
	private String nivell;
	private ButtonGroup group;
	private int countNextButton = 0;
	private String workingDir;
	private Double tempsRestant = 0.0;
	private int filesNivell;
	private int columnesNivell;
	private int nMines;
	private String tipusPartida;
	private JButton taulell[][];
	private int contadorBanderas = 0;
	private int intentos;
	private JTextPane timeLeft;
	private String[][] nivells;
	private JTextPane minesLeft;

	/**
	 * Launch the application.
	 * 
	 *
	
	 Funcio per executar nomes la vista sense utilitzar el hibernate, 
	 encara que es indispensable l'us de l'hibernate,
	 ja que sino no podra realitzar correctament les funcions de login i consultarNivell
	 
	/*public static void main(String[] args) {

		
		 * This is the most important part ofyour GUI app, never forget to
		 * schedule a job for your event dispatcher thread : by calling the
		 * function, method or constructor, responsible for creating and
		 * displaying your GUI.
		 
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JugarPartidaController controlador = new JugarPartidaController();
				new JugarPartidaView(controlador);
			}
		});
	}*/

	
	/*
	 * La creadora de la vista rep com a parametre el controlador de presentacio
	 * per tenir visibilitat es l'encarregada d'inicialitzar el fons de pantalla
	 * i cridar la funcio que crea les components del login
	 */
	public JugarPartidaView(JugarPartidaController controlador) {
		this.CtrlJugarPartida = controlador;
		jframe = new JFrame("Buscamines");
		workingDir = System.getProperty("user.dir");
		jframe.setSize(400, 400);
		jframe.setResizable(false);
		jframe.setLayout(new BorderLayout());
		ImageIcon icon = new ImageIcon(workingDir
				+ "//imagenes//background.jpg");
		Image img = icon.getImage();
		Image newimg = img.getScaledInstance(400, 400,
				java.awt.Image.SCALE_SMOOTH);
		ImageIcon newIcon = new ImageIcon(newimg);
		jframe.setContentPane(new JLabel(newIcon));
		jframe.setLayout(new FlowLayout());
		jframe.setLocationRelativeTo(null);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		prepareLogin();
		jframe.setVisible(true);

	}

	/*
	 * Funcio que inicialitza les components de la pantalla de login i la seva
	 * aparenza, tals com els camps d'username i contrasenya. Afegeix tambuien els
	 * listeners per a poder rebre esdeveniments
	 */
	public void prepareLogin() {

		userField = new JTextPane();

		userField.setFont(new Font("Arial", Font.BOLD, 14));
		userField.setForeground(Color.YELLOW);
		userField.setBounds(130, 29, 62, 23);
		jframe.getContentPane().add(userField);
		userField.setText("Usuari : ");
		userField.setOpaque(false);
		userField.setEditable(false);

		txtUser = new JTextField();
		txtUser.setBounds(200, 29, 86, 20);
		jframe.getContentPane().add(txtUser);

		txtPassword = new JTextPane();

		txtPassword.setFont(new Font("Arial", Font.BOLD, 14));
		txtPassword.setOpaque(false);
		txtPassword.setBounds(90, 117, 92, 20);

		txtPassword.setEditable(false);
		txtPassword.setForeground(Color.YELLOW);
		txtPassword.setText("Contrasenya :  ");
		jframe.getContentPane().add(txtPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(200, 117, 86, 20);
		jframe.getContentPane().add(passwordField);

		nextButton = new JButton("OK");
		nextButton.setBounds(90, 320, 100, 20);
		jframe.getContentPane().setLayout(null);
		jframe.getContentPane().add(nextButton);

		finishButton = new JButton("Cancelar");
		finishButton.setBounds(200, 320, 100, 20);
		jframe.getContentPane().add(finishButton);

		nextButton.addActionListener(this);
		finishButton.addActionListener(this);

	}

	/*
	 * Funcio encarregada de modificar la vista principal quan s'ha realitzat el
	 * login correctament. es l'encarregada d'obtenir els nivells disponibles al
	 * sistema, demanant-los al controlador de Presentacio A mes emmagatzema el
	 * nivell escollit per poder passar-lo a la seg�ent finestra, que crea la
	 * partida
	 */

	public void vistaNivells() {



		++countNextButton; // aquest comptador es per diferenciar a quina
							// finestra ens trobem,
							// ja que treballem amb una unica vista que es va
							// modificant

		if (nivells.length == 0) {
			showMessage("No hi ha nivells");
			jframe.dispose();
		} else {

			jframe.remove(passwordField);
			jframe.remove(txtPassword);
			jframe.remove(txtUser);
			jframe.remove(userField);
			jframe.setLocationRelativeTo(null);

			group = new ButtonGroup();

			// Creacio dels RadioButtons a partir dels nivells obtinguts. Li
			// afegim un listener que quan es activat enregistra
			// la informacio del nivell escollit al nivell actual, per a poder
			// consultar-lo en la seguent pantalla

			for (int i = 0; i < nivells.length; ++i) {
				radioButton = new JRadioButton();
				radioButton.setText(nivells[i][0] + ":" + nivells[i][1] + ","
						+ nivells[i][2] + "." + nivells[i][3]);
				radioButton.setBounds(20, 60 + i * 40, 300, 50);
				radioButton.setFont(new Font("Arial", Font.BOLD, 14));
				radioButton.setForeground(Color.WHITE);
				radioButton.setOpaque(false);
				
				// Accio pels JRadioButtons
				/* Cada cop que es selecciona un radioButton 
				 * es guarda en las variables tota la informacio dels nivell seleccionat */
				
				radioButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String s = e.getActionCommand();
						
						String aux = "";
						for (int i = 0; i < s.length(); ++i) {
							if (s.charAt(i) == ':') {
								nivell = aux;
								++i;
								aux = "";
							}
							if (s.charAt(i) == ',') {
								filesNivell = Integer.parseInt(aux);
								++i;
								aux = "";
							}
							if (s.charAt(i) == '.') {
								columnesNivell = Integer.parseInt(aux);
								++i;
								aux = "";
							}
							aux += s.charAt(i);

						}
						nMines = Integer.parseInt(aux);
					}
				});
				
				// Seleccionem el primer nivell per defecte
				if (i == 0)	radioButton.doClick();

				radioButton.setBackground(new Color(51, 153, 255));
				group.add(radioButton);
				jframe.getContentPane().add(radioButton);
			}

			jframe.validate();
			jframe.repaint();


		}

	}

	public void showMessage(String s) {
		JOptionPane.showMessageDialog(null, s);
	}

	public void actionPerformed(ActionEvent arg0) {

		String comando = arg0.getActionCommand();


		// Accio pel JButton OK
		if (comando == "OK") {

			if (countNextButton == 0) {

				// Comprova que els dos camps estan plens

				if (txtUser.getText().length() == 0
						|| passwordField.getPassword().length == 0) {
					showMessage("Un dels camps es buit");
					txtUser.setText("");
					passwordField.setText("");
				} else {
					try {

						// Autentifica el username i password introduits
						nivells = CtrlJugarPartida.pressOKLogin(
								txtUser.getText(),
								String.valueOf(passwordField.getPassword()));
						
						// Canvien la apariencia de la vista 
						vistaNivells();
					} catch (Exception e) {
						showMessage("Usuari o contrasenya incorrecte");
					}
				}
			} else if (countNextButton == 1) {

				// Es crea la partida i ens retorna el tipus de partida que es

				String tipus = CtrlJugarPartida.pressOKConsNivell(nivell);
				
				String[] aux = tipus.split(" ");
				tipusPartida = aux[0];
				
				// Canvien la apariencia de la vista 
				vistaFinal();

			}
		}
		// Accio pel JButton Cancelar
		if (comando == "Cancelar") {
			jframe.dispose();
		}
	}

	// Accio pel les caselles de la vista final, on es juga al joc del buscamines 
	public void mouseClicked(MouseEvent e) {

		// Agafo el JButton que on s'ha realitzat l'accio
		JButton b = (JButton) e.getSource();
		String[] aux = b.getName().split("\\.");
		int fila = Integer.parseInt(aux[0]);
		int columna = Integer.parseInt(aux[1]);
		
		// Si s'ha clicat amb el boto esquerra
		if (e.getButton() == MouseEvent.BUTTON1 && b.getIcon() == null) {

			ResultPartida result = null;
			ImageIcon icon = null;
			
			// Si la partida es del tipus throws, el timeLeft marca els intents que s'han realitzat
			if(tipusPartida.equals("throws")){
				++intentos;
				timeLeft.setText(tipusPartida + " :  " + intentos);
			}

			try {
				result = CtrlJugarPartida.descobrirCasella(fila,columna);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Si es perd
			
			if (result.isAcabada() && !result.isGuanyada()) {

				icon = new ImageIcon(workingDir + "//imagenes//mina.png");
				Image img = icon.getImage();
				Image newimg = img.getScaledInstance(b.getWidth(),
						b.getWidth(), java.awt.Image.SCALE_SMOOTH);
				icon = new ImageIcon(newimg);
				b.setIcon(icon);

				showMessage("Has perdut");
				jframe.dispose();
			}
			// Si es guanya
			if (result.isAcabada() && result.isGuanyada()) {
				showMessage("Has guanyat");
				jframe.dispose();
			}

			// Mientras es juga la partida
			if (!result.isAcabada()) {
				
				// Miro quines son les caselles que tinc de descubrir
				for (int i = 0; i < result.getCasellesDescobertes().size(); ++i) {
					int filaCasella =result.getCasellesDescobertes().get(i).getNumFila();
					int columnaCasella = result.getCasellesDescobertes().get(i).getNumCol();
					int numero =result.getCasellesDescobertes().get(i).getNumero();
					JButton aux1 = taulell[filaCasella][columnaCasella];
					
					/* Si la casella no te ninguna bomba al voltant, 
					 * es desabilita i es canvia el fons de la casella */
					
					if(numero == 0 /*&& aux1.getIcon() == null*/) {
						aux1.setForeground(Color.BLUE);
						aux1.setEnabled(false);
						aux1.setIcon(null);
					}
					/* Si la casella te alguna bomba al voltant, 
					 * es posa una image en la casella indicant el nombre de bombes que hi ha al voltant */
					
					if(numero != 0 /*&& aux1.getIcon() == null*/){
						icon = new ImageIcon(workingDir+ "//imagenes//"	+ numero + ".png");
						Image img = icon.getImage();
						Image newimg = img.getScaledInstance(b.getWidth(),
								b.getWidth(), java.awt.Image.SCALE_SMOOTH);
						icon = new ImageIcon(newimg);
						aux1.setIcon(icon);

					}
				}
			}
		}

		// Clic amb el boto dreta 

		if (e.getButton() == MouseEvent.BUTTON3) {
					
			boolean estadoButton = CtrlJugarPartida.isMarcada(fila,columna);
			boolean buttonDescubierto = CtrlJugarPartida.isDescoberta(fila,columna);
			
			/* Si la casella no te bandera i no esta descuberta, 
			 * es posa una bandera en la casella */
			
			if (!estadoButton && !buttonDescubierto) {
				ImageIcon icon = new ImageIcon(workingDir
						+ "//imagenes//bandera.png");
				Image img = icon.getImage();
				Image newimg = img.getScaledInstance(b.getWidth(),
						b.getWidth(), java.awt.Image.SCALE_SMOOTH);
				icon = new ImageIcon(newimg);

				b.setIcon(icon);

				try {
					
					CtrlJugarPartida.marcarCasella(fila,columna);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				++contadorBanderas;
			}
			
			
			/* Si la casella te bandera i no esta descuberta, 
			 * es treu la bandera de la casella */
			
			else if (estadoButton && !buttonDescubierto){
				b.setIcon(null);
				try {
					
					CtrlJugarPartida.desmarcarCasella(fila,columna);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				--contadorBanderas;
			}
			minesLeft.setText("Banderes restants: " + (nMines - contadorBanderas));
			
		}
	}

	private int min(int x, int y) {
		if (x < y)
			return x;
		else
			return y;
	}

	public void vistaFinal() {

		jframe.getContentPane().removeAll();
		jframe.setVisible(true);
		jframe.setBackground(Color.LIGHT_GRAY);
		jframe.setContentPane(new JLabel());

		int columnes = columnesNivell; // 9x9,16x16,16x30
		int files = filesNivell;

		if (columnes < 5 || files < 5)
			jframe.setSize(columnes * 80, files * 80);
		else if (columnes < 10 || files < 10)
			jframe.setSize(columnes * 40, files * 40);
		else
			jframe.setSize(min(columnes * 25, 600), min(files * 25, 600));

		minesLeft = new JTextPane();
		minesLeft.setFont(new Font("Arial", Font.BOLD, 14));
		minesLeft.setBounds(10, 20, 200, 20);
		minesLeft.setOpaque(false);
		minesLeft.setEditable(false);

		minesLeft.setText("Banderes restants: " + (nMines - contadorBanderas));
		jframe.getContentPane().add(minesLeft);

		timeLeft = new JTextPane();
		timeLeft.setFont(new Font("Arial", Font.BOLD, 14));
		timeLeft.setOpaque(false);
		timeLeft.setBounds(jframe.getWidth() - 140, 20, 250, 20);
		timeLeft.setEditable(false);
		timeLeft.setText(tipusPartida + " :  ");
		jframe.getContentPane().add(timeLeft);
		
		/* Si la partida es de tipus time, 
		 * es posa un contador de temps en segons  */
		if (tipusPartida.equals("time")) {
			Timer tiempo = new Timer(1000, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					++tempsRestant;
					timeLeft.setText(tipusPartida + ":" + tempsRestant);
				}
			});
			tiempo.start();
			tiempo.addActionListener(this);
		}

		taulell = new JButton[files][columnes];
		Container grid = new Container();
		grid.setLayout(new GridLayout(files, columnes));

		grid.setBounds(50, 50, jframe.getWidth() - 100,
				jframe.getHeight() - 100);

		for (int i = 0; i < files; ++i) {
			for (int j = 0; j < columnes; ++j) {
				taulell[i][j] = new JButton();
				taulell[i][j].addMouseListener(this);
				taulell[i][j].setName(i + "." + j);

				grid.add(taulell[i][j]);
			}
		}

		jframe.getContentPane().add(grid, BorderLayout.CENTER);

		jframe.validate();
		jframe.repaint();

	}
	
	/* Funcionalitats necesaries de declarar, ja que utilizem la classe MouseListener */

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
