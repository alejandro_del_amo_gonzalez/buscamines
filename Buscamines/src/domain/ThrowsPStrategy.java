package domain;
public class ThrowsPStrategy implements IPStrategy {
	//implementacio d'estrategia per tirades, nomes es crea una sola vegada
	private static ThrowsPStrategy instance;
	private int throwsMax;

	private ThrowsPStrategy() {
		throwsMax = Buscamines.getInstance().getThrowsMax();
	}

	public static ThrowsPStrategy getInstance() {
		if (instance == null)
			instance = new ThrowsPStrategy();
		return instance;
	}

	public int calcularPuntuacio(int time, int nombreTirades) {
		return throwsMax - nombreTirades;
	}

	public String getStrategy() {
		return "throws";
	}

}
