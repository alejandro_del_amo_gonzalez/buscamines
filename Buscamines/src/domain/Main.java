package domain;

import javax.swing.SwingUtilities;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import presentation.JugarPartidaController;
import presentation.JugarPartidaView;

public class Main {

	public static void main(String[] args) {
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.addAnnotatedClass(Nivell.class);
		config.addAnnotatedClass(Jugador.class);
		config.configure("hibernate.cfg.xml");

		new SchemaExport(config).create(true, true);

		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();

		String nom1 = "Facil";
		int files1 = 9;
		int columnes1 = 9;
		int mines1 = 10;
		Nivell facil = new Nivell(nom1, files1, columnes1, mines1);

		String nom2 = "Intermig";
		int files2 = 16;
		int columnes2 = 16;
		int mines2 = 40;
		Nivell intermig = new Nivell(nom2, files2, columnes2, mines2);

		String nom3 = "Dificil";
		int files3 = 16;
		int columnes3 = 30;
		int mines3 = 99;
		Nivell dificil = new Nivell(nom3, files3, columnes3, mines3);

		String nom = "Nom";
		String cognom = "Cognom";
		String user = "Usuari";
		String pwd = "Contrasenya";
		String email = "paufr94@gmail.com";
		Jugador jugador = new Jugador(nom, cognom, user, pwd, email);

		session.save(facil);
		session.save(intermig);
		session.save(dificil);
		session.save(jugador);
		session.getTransaction().commit();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JugarPartidaController controlador = new JugarPartidaController();
				new JugarPartidaView(controlador);
			}
		});
	}

}
