package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Nivell {

	private String nom;
	private int nombreCasellesxFila;
	private int nombreCasellesxColumna;
	private int nombreMines;

	public Nivell(String nom, int files, int columnes, int mines) {
		this.nom = nom;
		this.nombreCasellesxColumna = files;
		this.nombreCasellesxFila = columnes;
		this.nombreMines = mines;
	}

	@Id
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(nullable = false)
	public int getNombreCasellesxFila() {
		return nombreCasellesxFila;
	}

	public void setNombreCasellesxFila(int nombreCasellesxFila) {
		this.nombreCasellesxFila = nombreCasellesxFila;
	}

	@Column(nullable = false)
	public int getNombreCasellesxColumna() {
		return nombreCasellesxColumna;
	}

	public void setNombreCasellesxColumna(int nombreCasellesxColumna) {
		this.nombreCasellesxColumna = nombreCasellesxColumna;
	}

	@Column(nullable = false)
	public int getNombreMines() {
		return nombreMines;
	}

	public void setNombreMines(int nombreMines) {
		this.nombreMines = nombreMines;
	}

}