package domain;
import java.util.Random;

public class TipusPuntFactory {
	//factoria d'estrategies de puntuacio
	Random rand = new Random();
	private static TipusPuntFactory instance;

	private TipusPuntFactory() {
	}

	public static TipusPuntFactory getInstance() {
		if (instance == null)
			instance = new TipusPuntFactory();
		return instance;
	}
	//retorna una instancia d'estrategia de temps o de tirades per criteri random
	public IPStrategy getIPStrategy() {
		int random = rand.nextInt(2);
		if (random == 0)
			return TimePStrategy.getInstance();
		else
			return ThrowsPStrategy.getInstance();
	}

}
