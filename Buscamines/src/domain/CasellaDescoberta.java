package domain;

public class CasellaDescoberta {

	private int numFila;
	private int numCol;
	private int numero;

	public CasellaDescoberta(int numFila, int numCol, int numero) {
		this.numFila = numFila;
		this.numCol = numCol;
		this.numero = numero;
	}

	public int getNumFila() {
		return numFila;
	}

	public int getNumCol() {
		return numCol;
	}

	public int getNumero() {
		return numero;
	}

}
